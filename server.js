const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const db = 'mongodb://wamz:27017/test';

// configuring app to use bodyParser
// this will let us get the data from a POST request
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

const port = 3000;

module.exports = {"app": app, "port": port, "db": db};
