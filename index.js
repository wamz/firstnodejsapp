const server = require('./server');
var mongoose = require('mongoose');
var Person = require('./models/person');
const app = server.app;

// connecting to db
mongoose.connect(server.db);

// get all people
app.get('/people', (req, res) => {
    Person.find((error, people) => {
        if(error)
            res.send(error);
        res.json({people});
    });
});

// get one person
app.get('/person/:id', (req, res) => {
    Person.findById(req.params.id, (error, person) => {
        if(error)
            res.send(error);
        res.json({person});
    });
});

// create a person
app.post('/person', (req, res) => {
    var person = new Person();
    person.name = req.body.name;
    person.profession = req.body.profession;

    person.save((error) => {
        if(error)
            res.send(error);
        res.json({message: "Person Created!"});
    });
});

// update person
app.put('/person/:id', (req, res) => {
    Person.findById(req.params.id, (error, person) => {
        if(error)
            res.send(error);
        
        person.name = req.body.name;
        person.profession = req.body.profession;

        person.save((error) => {
            if(error)
                res.send(error);
            res.json({message: "Person Updated!"});
        });
    });
});

// delete person
app.delete('/person/:id', (req, res) => {
    Person.remove({_id: req.params.id}, (error, person) => {
        if(error)
            res.send(error);
        res.json({message: "Person Deleted!"});
    });
});

app.listen(server.port);
console.log("Server Started!");